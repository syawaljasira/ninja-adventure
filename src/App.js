import React from 'react';
import { Routes, Route } from 'react-router-dom';

import ComicPage from './components/ComicPage';
import LandingPage from './components/LandingPage';
import { StoreProvider } from './utils/Store';

function App() {
  return (
    <StoreProvider>
      <Routes>
        <Route element={<LandingPage />} path="/" />
        <Route element={<ComicPage />} path="/comic" />
      </Routes>
    </StoreProvider>
  );
}

export default App;
