import { createContext, useReducer } from 'react';

export const Store = createContext();

const initialState = {
  answer1: 'had been feeding',
  answer3: 'had been sleeping',
  answer5: 'has given',
  answer7: 'has cooked',
  question: '',
  panelPass: '',
  selectPass: '',
  panel: {},
  story: {},
};

function reducer(state, action) {
  switch (action.type) {
    case 'QUESTION_REQUEST':
      return { ...state, question: action.payload };
    case 'QUESTION_PASS':
      return { ...state, panelPass: action.payload };
    case 'ANSWER_FALSE':
      return { ...state, panelPass: 'answer_false' };
    case 'SELECT_REQUEST':
      return {
        ...state,
        selectPass: action.payload,
        question: 'pending',
        panelPass: 'pending',
      };
    case 'SELECT_PASS': {
      const story = { ...state.story, ...action.payload.story };
      return {
        ...state,
        selectPass: 'pending',
        panel: { ...state.panel, ...action.payload.panels },
        story,
      };
    }
    case 'RESET':
      return initialState;
    default:
      return state;
  }
}

export function StoreProvider({ children }) {
  const [state, dispatch] = useReducer(reducer, initialState);
  const value = { state, dispatch };
  return <Store.Provider value={value}>{children}</Store.Provider>;
}
