import React, { useContext } from 'react';
import { Store } from '../utils/Store';

const SelectMonster = ({ selectHandler }) => {
  const { state } = useContext(Store);

  const { story } = state;
  return (
    <section className="flex flex-wrap z-10 px-6 space-y-5">
      <span className="w-full text-2xl text-blue-400">Develop your story</span>
      <span className="w-full text-base text-blue-400">
        What is the weapon in the box?
      </span>
      <div className="flex space-x-3 mx-auto">
        {story.weapon === 'katana_boy' ? (
          <>
            <button
              onClick={() => selectHandler('katana_kawauso')}
              className="w-6/12 border-4 focus:border-purple-400 rounded-lg"
            >
              <img src="/images/optionReel/kawauso.jpg" alt="Kawauso Monster" />
            </button>
            <button
              onClick={() => selectHandler('katana_kitsune')}
              className="w-6/12 border-4 focus:border-purple-400 rounded-lg"
            >
              <img src="/images/optionReel/kitsune.jpg" alt="Kitsune Monster" />
            </button>
          </>
        ) : (
          <>
            <button
              onClick={() => selectHandler('shuriken_kawauso')}
              className="w-6/12 border-4 focus:border-purple-400 rounded-lg"
            >
              <img src="/images/optionReel/kawauso.jpg" alt="Kawauso Monster" />
            </button>
            <button
              onClick={() => selectHandler('shuriken_kitsune')}
              className="w-6/12 border-4 focus:border-purple-400 rounded-lg"
            >
              <img src="/images/optionReel/kitsune.jpg" alt="Kitsune Monster" />
            </button>
          </>
        )}
      </div>
    </section>
  );
};

export default SelectMonster;
