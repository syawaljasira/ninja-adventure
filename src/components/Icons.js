import React from 'react';

export function BtnStart({ className, style }) {
  return (
    <img
      className={className}
      style={style}
      src="/images/components/btnStart.png"
      alt="Button Start"
    />
  );
}

export function BtnNext({ className, style, onClick }) {
  return (
    <img
      onClick={onClick}
      className={className}
      style={style}
      src="/images/components/btnNext.png"
      alt="Button Next"
    />
  );
}

export function BtnSubmit({ className, style, onClick }) {
  return (
    <img
      onClick={onClick}
      className={className}
      style={style}
      src="/images/components/btnSubmit.png"
      alt="Button Submit"
    />
  );
}

export function PenIcon({ className, style, bgColor, onClick }) {
  return (
    <div
      onClick={onClick}
      className={`w-16 h-16 flex justify-center items-center rounded-full ${bgColor}`}
    >
      <img
        className={className}
        style={style}
        src="/images/components/icon_pen.png"
        alt="Pen Icon"
      />
    </div>
  );
}

export function QuestionIcon({ className, style }) {
  return (
    <div className="w-16 h-16 flex justify-center items-center bg-gray-400 rounded-full">
      <img
        className={className}
        style={style}
        src="/images/components/icon_question.png"
        alt="Question Icon"
      />
    </div>
  );
}

export function LampIcon({ className, style }) {
  return (
    <div className="w-16 h-16 flex justify-center items-center bg-gray-400 rounded-full">
      <img
        className={className}
        style={style}
        src="/images/components/icon_lamp.png"
        alt="Lamp Icon"
      />
    </div>
  );
}

export function LockIcon({ className, style }) {
  return (
    <img
      className={className}
      style={style}
      src="/images/components/icon_lock.png"
      alt="Lock Icon"
    />
  );
}

export function Number({ children, right }) {
  return (
    <span
      className={`absolute top-1 text-xl text-white ${
        right ? right : 'left-2'
      }`}
    >
      {children}
    </span>
  );
}
