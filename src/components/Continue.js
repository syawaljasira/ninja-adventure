import React from 'react';

const Continue = () => {
  return (
    <section className="absolute right-8 w-4/12 py-5 px-8 mt-12 ml-8 flex flex-wrap justify-center text-white font-baloo space-y-2">
      <div className="w-full text-3xl text-center mb-5">
        Continue to develop the next panel.
      </div>
    </section>
  );
};

export default Continue;
