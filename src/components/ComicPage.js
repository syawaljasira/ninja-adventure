import React, { useContext } from 'react';
import { Store } from '../utils/Store';
import Continue from './Continue';
import Finish from './Finish';
import { LockIcon, Number, PenIcon } from './Icons';
import PlayGuide from './PlayGuide';
import QuestionScroll from './QuestionScroll';
import SelectScroll from './SelectScroll';

const ComicPage = () => {
  const { state, dispatch } = useContext(Store);

  const panelQuestionHandler = (panel) => {
    dispatch({ type: 'QUESTION_REQUEST', payload: panel });
  };

  return (
    <div className="relative max-h-screen max-w-screen">
      <div className="absolute">
        <img
          className="w-full h-full"
          src="/images/components/bgComicPage.jpg"
          alt="Comic Page"
        />
      </div>
      <div className="absolute left-8 w-7/12 h-screen py-4 mt-12 flex flex-wrap justify-center bg-gray-200 text-white px-8 font-baloo space-y-2 overflow-scroll">
        <div className="w-full text-gray-800 font-gang text-5xl text-center pt-4 pb-2">
          Ninja Adventure
        </div>
        <div className="w-full flex h-40 justify-center space-x-2">
          <div
            className={`relative w-6/12 flex flex-wrap justify-center items-center border-2 border-black ${
              state.panel.panel1 ? '' : 'py-4'
            } ${
              state.selectPass === 'request1' ? 'bg-green-10' : 'bg-gray-10'
            }`}
          >
            {state.selectPass === 'request1' ? (
              <>
                <Number children="1" />
                <div className="text-6xl">...</div>
              </>
            ) : state.panel.panel1 ? (
              <div className="h-full w-full flex">
                <img
                  className="w-full object-fill"
                  src={state.panel.panel1.image}
                  alt="Panel 1 Story"
                />
              </div>
            ) : (
              <>
                <Number children="1" />
                <PenIcon
                  onClick={() => panelQuestionHandler('panel_1')}
                  className="w-auto h-10"
                  bgColor="bg-gray-300"
                />
                <span className="w-full text-center text-lg">
                  Select the character
                </span>
              </>
            )}
          </div>
          <div className="w-2/12 relative flex justify-center items-center bg-gray-10 border-2 border-black">
            {!state.panel.panel2 ? (
              <>
                <Number children="2" />
                <LockIcon className="w-auto h-10" />
              </>
            ) : (
              <div className="h-full w-full flex">
                <img
                  className="w-full"
                  src={state.panel.panel2.image}
                  alt="Panel 2 Story"
                />
              </div>
            )}
          </div>
          <div
            className={`w-4/12 relative flex flex-wrap justify-center items-center bg-gray-10 border-2 border-black ${
              state.panel.panel3 ? '' : 'py-4'
            } ${
              state.selectPass === 'request3' ? 'bg-green-10' : 'bg-gray-10'
            }`}
          >
            {state.selectPass === 'request3' ? (
              <>
                <Number children="3" />
                <div className="text-6xl">...</div>
              </>
            ) : state.panel.panel3 ? (
              <div className="h-full w-full flex">
                <img
                  className="w-full object-fill"
                  src={state.panel.panel3.image}
                  alt="Panel 3 Story"
                />
              </div>
            ) : state.panel.panel1 ? (
              <>
                <Number children="3" />
                <PenIcon
                  onClick={() => panelQuestionHandler('panel_3')}
                  className="w-auto h-10"
                  bgColor="bg-gray-300"
                />
                <span className="w-full text-center text-lg">
                  What is in the box?
                </span>
              </>
            ) : (
              <>
                <Number children="3" />
                <LockIcon className="w-auto h-10" />
              </>
            )}
          </div>
        </div>
        <div className="w-full flex h-40 justify-center space-x-2">
          <div className="w-3/12 relative flex justify-center items-center bg-gray-10 border-2 border-black">
            {!state.panel.panel4 ? (
              <>
                <Number children="4" />
                <LockIcon className="w-auto h-10" />
              </>
            ) : (
              <div className="h-full w-full flex">
                <img
                  className="w-full"
                  src={state.panel.panel4.image}
                  alt="Panel 4 Story"
                />
              </div>
            )}
          </div>
          <div
            className={`w-9/12 relative flex flex-wrap justify-center items-center bg-gray-10 border-2 border-black ${
              state.panel.panel5 ? '' : 'py-4'
            } ${
              state.selectPass === 'request1' ? 'bg-green-10' : 'bg-gray-10'
            }`}
          >
            {state.selectPass === 'request5' ? (
              <>
                <Number children="5" />
                <div className="text-6xl">...</div>
              </>
            ) : state.panel.panel5 ? (
              <div className="h-full w-full flex">
                <img
                  className="w-full object-fill"
                  src={state.panel.panel5.image}
                  alt="Panel 5 Story"
                />
              </div>
            ) : state.panel.panel4 ? (
              <>
                <Number children="5" />
                <PenIcon
                  onClick={() => panelQuestionHandler('panel_5')}
                  className="w-auto h-10"
                  bgColor="bg-gray-300"
                />
                <span className="w-full text-center text-lg">
                  Where does the ninja go?
                </span>
              </>
            ) : (
              <>
                <Number children="5" />
                <LockIcon className="w-auto h-10" />
              </>
            )}
          </div>
        </div>
        <div className="w-full flex h-80 justify-center">
          <div className="w-9/12 flex flex-wrap">
            <div className="w-full flex ">
              <div className="w-4/12 relative flex justify-center items-center bg-gray-10 mb-2 mr-2 border-2 border-black">
                {!state.panel.panel6 ? (
                  <>
                    <Number children="6" />
                    <LockIcon className="w-auto h-10" />
                  </>
                ) : (
                  <div className="h-full w-full flex">
                    <img
                      className="w-full"
                      src={state.panel.panel6.image}
                      alt="Panel 6 Story"
                    />
                  </div>
                )}
              </div>
              <div
                className={`w-8/12 relative flex flex-wrap justify-center items-center bg-gray-10 mb-2 border-2 border-black ${
                  state.panel.panel7 ? '' : 'py-8'
                } ${
                  state.selectPass === 'request1' ? 'bg-green-10' : 'bg-gray-10'
                }`}
                style={{
                  clipPath: `polygon(0 0, 100% 0, 95% 100%, 0 100%)`,
                }}
              >
                {state.selectPass === 'request7' ? (
                  <>
                    <Number children="7" />
                    <div className="text-6xl">...</div>
                  </>
                ) : state.panel.panel7 ? (
                  <div className="h-full w-full flex">
                    <img
                      className="w-full object-fill"
                      src={state.panel.panel7.image}
                      alt="Panel 7 Story"
                    />
                  </div>
                ) : state.panel.panel6 ? (
                  <>
                    <Number children="7" />
                    <PenIcon
                      onClick={() => panelQuestionHandler('panel_7')}
                      className="w-auto h-10"
                      bgColor="bg-gray-300"
                    />
                    <span className="w-full text-center text-lg">
                      What is the monster?
                    </span>
                  </>
                ) : (
                  <>
                    <Number children="7" />
                    <LockIcon className="w-auto h-10" />
                  </>
                )}
              </div>
            </div>
            <div
              className="w-full flex"
              style={{
                clipPath: `polygon(0 0, 96.5% 0, 94.6% 100%, 0 100%)`,
              }}
            >
              <div className="w-full relative flex justify-center items-center bg-gray-10 border-2 border-black">
                {!state.panel.panel8 ? (
                  <>
                    <Number children="8" />
                    <LockIcon className="w-auto h-10" />
                  </>
                ) : (
                  <div className="h-full w-full flex">
                    <img
                      className="w-full"
                      src={state.panel.panel8.image}
                      alt="Panel 8 Story"
                    />
                  </div>
                )}
              </div>
            </div>
          </div>
          <div
            className={`w-3/12 relative flex justify-center items-center bg-gray-10 border-2 border-black ${
              state.panel.panel9 ? 'h-96' : 'h-80'
            }`}
            style={{ clipPath: `polygon(17% 0, 100% 0, 100% 100%, 0 100%)` }}
          >
            {!state.panel.panel9 ? (
              <>
                <Number children="9" />
                <LockIcon className="w-auto h-10" />
              </>
            ) : (
              <div className="h-full w-full flex">
                <img
                  className="w-full"
                  src={state.panel.panel9.image}
                  alt="Panel 9 Story"
                />
              </div>
            )}
          </div>
        </div>
      </div>
      {!state.question ? (
        <PlayGuide />
      ) : state.question === 'panel_1' ? (
        <QuestionScroll
          request="request1"
          placeholder="feed"
          sentence1="My neighbor"
          sentence2="the stray cats for the past few months. She was told to stop
          two days ago."
        />
      ) : state.question === 'panel_3' ? (
        <QuestionScroll
          request="request3"
          placeholder="sleep"
          sentence1="My sister"
          sentence2="until the sound of the vacuum cleaner woke her up."
        />
      ) : state.question === 'panel_5' ? (
        <QuestionScroll
          request="request5"
          placeholder="give"
          sentence1='"My friend gave me this bar of chocolate!" Minh exclaimed. Minh exclaimed that his friend'
          sentence2="him that bar of chocolate."
        />
      ) : state.question === 'panel_7' ? (
        <QuestionScroll
          request="request7"
          placeholder="cook"
          sentence1="My mother"
          sentence2="my favorite food."
        />
      ) : state.question === 'pending' && state.panel.panel9 ? (
        <Finish />
      ) : (
        <Continue />
      )}
      {state.selectPass === 'request1' ? (
        <SelectScroll request="character" />
      ) : state.selectPass === 'request3' ? (
        <SelectScroll request="weapon" />
      ) : state.selectPass === 'request5' ? (
        <SelectScroll request="ninja_path" />
      ) : state.selectPass === 'request7' ? (
        <SelectScroll request="monster" />
      ) : (
        <span>Undefined</span>
      )}
    </div>
  );
};

export default ComicPage;
