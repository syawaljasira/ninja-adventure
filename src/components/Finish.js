import React from 'react';
import { Link } from 'react-router-dom';

const Finish = () => {
  return (
    <section className="absolute right-8 w-4/12 py-5 px-8 mt-12 ml-8 flex flex-wrap justify-center text-white font-baloo space-y-2">
      <div className="w-full text-3xl text-center mb-8">
        Great, you have completed your very own comic strip!
      </div>
      <Link to="/">
        <button className="bg-yellow-600 hover:bg-yellow-500 uppercase text-yellow-200 text-2xl py-1 px-6 rounded-3xl">
          Home
        </button>
      </Link>
    </section>
  );
};

export default Finish;
