import React, { useContext } from 'react';
import { Store } from '../utils/Store';
import SelectCharacter from './SelectCharacter';
import SelectNinjaPath from './SelectNinjaPath';
import SelectWeapon from './SelectWeapon';
import SelectMonster from './SelectMonster';

const SelectScroll = ({ request }) => {
  const { dispatch } = useContext(Store);

  const selectHandler = (option) => {
    if (option === 'boy') {
      dispatch({
        type: 'SELECT_PASS',
        payload: {
          story: {
            character: 'boy',
          },
          panels: {
            panel1: {
              image: '/images/ninjaboy/panel1.jpg',
            },
            panel2: {
              image: '/images/ninjaboy/panel2.jpg',
            },
          },
        },
      });
    }
    if (option === 'katana_boy') {
      dispatch({
        type: 'SELECT_PASS',
        payload: {
          story: {
            weapon: 'katana_boy',
          },
          panels: {
            panel3: {
              image: '/images/ninjaboy/panel3_katana.jpg',
            },
            panel4: {
              image: '/images/ninjaboy/panel4_katana.jpg',
            },
          },
        },
      });
    }
    if (option === 'shuriken_boy') {
      dispatch({
        type: 'SELECT_PASS',
        payload: {
          story: {
            weapon: 'shuriken_boy',
          },
          panels: {
            panel3: {
              image: '/images/ninjaboy/panel3_shuriken.jpg',
            },
            panel4: {
              image: '/images/ninjaboy/panel4_shuriken.jpg',
            },
          },
        },
      });
    }
    if (option === 'katana_town') {
      dispatch({
        type: 'SELECT_PASS',
        payload: {
          story: {
            ninja_path: 'katana_town',
          },
          panels: {
            panel5: {
              image: '/images/ninjaboy/panel5_katana_town.jpg',
            },
            panel6: {
              image: '/images/ninjaboy/panel6_town.jpg',
            },
          },
        },
      });
    }
    if (option === 'shuriken_town') {
      dispatch({
        type: 'SELECT_PASS',
        payload: {
          story: {
            ninja_path: 'shuriken_town',
          },
          panels: {
            panel5: {
              image: '/images/ninjaboy/panel5_shuriken_town.jpg',
            },
            panel6: {
              image: '/images/ninjaboy/panel6_town.jpg',
            },
          },
        },
      });
    }
    if (option === 'katana_woods') {
      dispatch({
        type: 'SELECT_PASS',
        payload: {
          story: {
            ninja_path: 'katana_woods',
          },
          panels: {
            panel5: {
              image: '/images/ninjaboy/panel5_katana_woods.jpg',
            },
            panel6: {
              image: '/images/ninjaboy/panel6_woods.jpg',
            },
          },
        },
      });
    }
    if (option === 'shuriken_woods') {
      dispatch({
        type: 'SELECT_PASS',
        payload: {
          story: {
            ninja_path: 'shuriken_woods',
          },
          panels: {
            panel5: {
              image: '/images/ninjaboy/panel5_shuriken_woods.jpg',
            },
            panel6: {
              image: '/images/ninjaboy/panel6_woods.jpg',
            },
          },
        },
      });
    }
    if (option === 'katana_kawauso') {
      dispatch({
        type: 'SELECT_PASS',
        payload: {
          story: {
            monster: 'katana_kawauso',
          },
          panels: {
            panel7: {
              image: '/images/ninjaboy/panel7_katana_kawauso.jpg',
            },
            panel8: {
              image: '/images/ninjaboy/panel8_slash_kawauso.jpg',
            },
            panel9: {
              image: '/images/ninjaboy/panel9_with_kawauso.jpg',
            },
          },
        },
      });
    }
    if (option === 'shuriken_kawauso') {
      dispatch({
        type: 'SELECT_PASS',
        payload: {
          story: {
            monster: 'shuriken_kawauso',
          },
          panels: {
            panel7: {
              image: '/images/ninjaboy/panel7_shuriken_kawauso.jpg',
            },
            panel8: {
              image: '/images/ninjaboy/panel8_slash_kawauso.jpg',
            },
            panel9: {
              image: '/images/ninjaboy/panel9_with_kawauso.jpg',
            },
          },
        },
      });
    }
    if (option === 'katana_kitsune') {
      dispatch({
        type: 'SELECT_PASS',
        payload: {
          story: {
            monster: 'katana_kitsune',
          },
          panels: {
            panel7: {
              image: '/images/ninjaboy/panel7_katana_kitsune.jpg',
            },
            panel8: {
              image: '/images/ninjaboy/panel8_slash_kawauso.jpg',
            },
            panel9: {
              image: '/images/ninjaboy/panel9_with_kawauso.jpg',
            },
          },
        },
      });
    }
    if (option === 'shuriken_kitsune') {
      dispatch({
        type: 'SELECT_PASS',
        payload: {
          story: {
            monster: 'shuriken_kitsune',
          },
          panels: {
            panel7: {
              image: '/images/ninjaboy/panel7_shuriken_kitsune.jpg',
            },
            panel8: {
              image: '/images/ninjaboy/panel8_slash_kawauso.jpg',
            },
            panel9: {
              image: '/images/ninjaboy/panel9_with_kawauso.jpg',
            },
          },
        },
      });
    }
    if (option === 'girl') {
      dispatch({
        type: 'SELECT_PASS',
        payload: {
          story: {
            character: 'girl',
          },
          panels: {
            panel1: {
              image: '/images/ninjagirl/panel1.jpg',
            },
            panel2: {
              image: '/images/ninjaboy/panel2.jpg',
            },
          },
        },
      });
    }
    if (option === 'katana_girl') {
      dispatch({
        type: 'SELECT_PASS',
        payload: {
          story: {
            weapon: 'katana_girl',
          },
          panels: {
            panel3: {
              image: '/images/ninjagirl/panel3_katana.jpg',
            },
            panel4: {
              image: '/images/ninjagirl/panel4_katana.jpg',
            },
          },
        },
      });
    }
    if (option === 'shuriken_girl') {
      dispatch({
        type: 'SELECT_PASS',
        payload: {
          story: {
            weapon: 'shuriken_girl',
          },
          panels: {
            panel3: {
              image: '/images/ninjagirl/panel3_shuriken.jpg',
            },
            panel4: {
              image: '/images/ninjagirl/panel4_shuriken.jpg',
            },
          },
        },
      });
    }
  };

  return (
    <section className="absolute right-8 w-4/12 px-8 ml-8 flex flex-wrap justify-center text-white font-baloo space-y-2">
      <img
        className="absolute px-4 z-0 -mt-16"
        src="/images/components/purple_scroll.png"
        alt="Selection Scroll"
      />
      {request === 'character' ? (
        <SelectCharacter selectHandler={selectHandler} />
      ) : request === 'weapon' ? (
        <SelectWeapon selectHandler={selectHandler} />
      ) : request === 'ninja_path' ? (
        <SelectNinjaPath selectHandler={selectHandler} />
      ) : request === 'monster' ? (
        <SelectMonster selectHandler={selectHandler} />
      ) : (
        <span></span>
      )}
    </section>
  );
};

export default SelectScroll;
