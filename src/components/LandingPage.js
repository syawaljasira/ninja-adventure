import React, { useContext } from 'react';
import Fire from './Fire';
import { Link, useNavigate } from 'react-router-dom';
import { BtnStart } from './Icons';
import { Store } from '../utils/Store';

const LandingPage = () => {
  const { state, dispatch } = useContext(Store);
  const navigate = useNavigate();

  const playAgainHandler = () => {
    dispatch({ type: 'RESET' });
    navigate('/comic');
  };

  return (
    <div className="relative max-h-screen max-w-screen">
      <div className="absolute">
        <img
          className="w-full h-full"
          src="/images/components/bgHomePage.jpg"
          alt="Landing Page"
        />
      </div>
      {state.panel.panel9 ? (
        <button
          onClick={playAgainHandler}
          className="absolute z-20 w-44 bg-yellow-600 hover:bg-yellow-500 uppercase py-1 px-4 text-2xl font-baloo text-yellow-200 rounded-2xl -my-8"
          style={{ left: '43.5%', top: '23rem' }}
        >
          Play Again
        </button>
      ) : (
        <span></span>
      )}
      <Link to="comic">
        <BtnStart
          className="absolute z-20 w-auto cursor-pointer"
          style={{ left: '44%', top: '25rem', height: '4.4rem' }}
        />
      </Link>
      <Fire />
    </div>
  );
};

export default LandingPage;
