import React from 'react';

const SelectCharacter = ({ selectHandler }) => {
  return (
    <section className="flex flex-wrap z-10 px-6 space-y-5">
      <span className="w-full text-2xl text-blue-400">Set your story</span>
      <span className="w-full text-base text-blue-400">
        Who is the main character in your story?
      </span>
      <div className="flex space-x-3 mx-auto">
        <button
          onClick={() => selectHandler('boy')}
          className="w-6/12 border-4 focus:border-purple-400 rounded-lg"
        >
          <img
            src="/images/optionReel/character_boy.jpg"
            alt="Ninja Boy Character"
          />
        </button>
        <button
          onClick={() => selectHandler('girl')}
          className="w-6/12 border-4 focus:border-purple-400 rounded-lg"
        >
          <img
            src="/images/optionReel/character_girl.jpg"
            alt="Ninja Boy Character"
          />
        </button>
      </div>
    </section>
  );
};

export default SelectCharacter;
