import React, { useContext, useState } from 'react';
import { Store } from '../utils/Store';
import { BtnNext, BtnSubmit } from './Icons';

const QuestionScroll = ({ request, sentence1, sentence2, placeholder }) => {
  const { state, dispatch } = useContext(Store);
  const [userAnswer, setUserAnswer] = useState('');

  const submitHandler = () => {
    if (
      userAnswer.toLowerCase().includes(state.answer1) ||
      userAnswer.toLowerCase().includes(state.answer3) ||
      userAnswer.toLowerCase().includes(state.answer5) ||
      userAnswer.toLowerCase().includes(state.answer7)
    ) {
      dispatch({ type: 'QUESTION_PASS', payload: 'pass' });
    } else {
      dispatch({ type: 'ANSWER_FALSE' });
    }
  };

  const nextHandler = () => {
    dispatch({ type: 'SELECT_REQUEST', payload: request });
  };
  console.log(state);

  return (
    <>
      <section className="absolute right-8 w-4/12 px-8 ml-8 flex flex-wrap justify-center text-white font-baloo space-y-2">
        <img
          className="absolute px-4 z-0 -mt-16"
          src="/images/components/red_scroll.png"
          alt="Question Scroll"
        />
        <section className="flex flex-wrap z-10 px-6 space-y-5">
          <span className="w-full text-2xl text-red-400">
            Solve this question to proceed!
          </span>
          <span className="w-full text-base text-red-400">
            Fill in the blank with answer the past perfect or the past perfect
            continous form of the word.
          </span>
          <form className="w-full text-xl text-gray-800 tracking-wide leading-relaxed">
            {sentence1} &nbsp;
            <input
              className={`w-8/12 rounded-2xl border-2 border-yellow-400 outline-none text-lg px-2 ${
                state.panelPass === '' || state.panelPass === 'pending'
                  ? ''
                  : state.panelPass === 'answer_false'
                  ? 'bg-red-300 bg-opacity-80'
                  : 'bg-green-400 bg-opacity-50'
              }`}
              disabled={
                state.panelPass === '' || state.panelPass === 'pending'
                  ? false
                  : state.panelPass === 'answer_false'
                  ? false
                  : true
              }
              type="text"
              name="answer"
              id="answer"
              placeholder={placeholder}
              onChange={(e) => setUserAnswer(e.target.value)}
            />
            &nbsp; {sentence2}
          </form>
        </section>
        {state.panelPass === '' ||
        state.panelPass === 'pending' ||
        state.panelPass === 'answer_false' ? (
          <BtnSubmit
            onClick={submitHandler}
            className="relative z-20 top-16 left-24 w-auto h-14 cursor-pointer"
          />
        ) : (
          <>
            <BtnNext
              onClick={nextHandler}
              className="relative z-20 top-16 left-24 w-auto h-14 cursor-pointer"
            />
          </>
        )}
      </section>
      {state.panelPass === 'answer_false' ? (
        <span className="absolute top-80 mt-52 text-white font-baloo text-2xl right-24">
          Sorry, your answer is wrong.
        </span>
      ) : state.panelPass === '' || state.panelPass === 'pending' ? (
        <span></span>
      ) : (
        <span className="absolute top-80 mt-52 text-white font-baloo text-2xl right-32">
          Great, you got it right!
        </span>
      )}
    </>
  );
};

export default QuestionScroll;
