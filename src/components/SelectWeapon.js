import React, { useContext } from 'react';
import { Store } from '../utils/Store';

const SelectWeapon = ({ selectHandler }) => {
  const { state } = useContext(Store);

  const { story } = state;

  console.log(story);
  return (
    <section className="flex flex-wrap z-10 px-6 space-y-5">
      <span className="w-full text-2xl text-blue-400">Develop your story</span>
      <span className="w-full text-base text-blue-400">
        What is the weapon in the box?
      </span>
      <div className="flex space-x-3 mx-auto">
        <button
          onClick={() =>
            selectHandler(
              story.character === 'boy' ? 'katana_boy' : 'katana_girl'
            )
          }
          className="w-6/12 border-4 focus:border-purple-400 rounded-lg"
        >
          <img src="/images/optionReel/katana.jpg" alt="Katana Ninja Weapon" />
        </button>
        <button
          onClick={() =>
            selectHandler(
              story.character === 'boy' ? 'shuriken_boy' : 'shuriken_girl'
            )
          }
          className="w-6/12 border-4 focus:border-purple-400 rounded-lg"
        >
          <img
            src="/images/optionReel/shuriken.jpg"
            alt="Shuriken Ninja Weapon"
          />
        </button>
      </div>
    </section>
  );
};

export default SelectWeapon;
