import React, { useEffect, useRef } from 'react';
import lottie from 'lottie-web';

import fireAnimation from '../lottie/fire.json';

const Fire = () => {
  const anim = useRef(null);

  useEffect(() => {
    lottie.loadAnimation({
      container: anim.current,
      renderer: 'svg',
      loop: true,
      autoplay: true,
      animationData: fireAnimation,
    });
  }, []);
  return (
    <div
      className="w-auto absolute h-80 mr-16 top-96 right-1/3"
      ref={anim}
    ></div>
  );
};

export default Fire;
