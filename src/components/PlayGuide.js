import React from 'react';
import { LampIcon, PenIcon, QuestionIcon } from './Icons';

const PlayGuide = () => {
  return (
    <section className="absolute right-8 w-4/12 py-5 px-8 mt-12 ml-8 flex flex-wrap justify-center text-white font-baloo space-y-2">
      <div className="w-full text-3xl text-center mb-5">
        Make your own comic!
      </div>
      <div className="w-full flex flex-wrap tracking-wide space-y-8 px-2">
        <div className="w-full flex items-center space-x-6">
          <PenIcon className="w-auto h-10" bgColor="bg-gray-400" />
          <div className="w-9/12 text-lg">
            Hit the create button. <br />
            You can only go by the order.
          </div>
        </div>
        <div className="w-full flex items-center space-x-6">
          <QuestionIcon className="w-auto h-10" />
          <div className="w-9/12 text-lg">
            Answer a question. <br /> You can create the content only when you
            got it right.
          </div>
        </div>
        <div className="w-full flex items-center space-x-6">
          <LampIcon className="w-auto h-10" />
          <div className="w-9/12 text-lg">
            Configure your story. <br /> Your selection will appear on the
            panel.
          </div>
        </div>
      </div>
    </section>
  );
};

export default PlayGuide;
