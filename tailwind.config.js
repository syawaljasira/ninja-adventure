module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        baloo: ['Baloo', 'sans-serif'],
        gang: ['GangofThreeRegular', 'sans-serif'],
      },
      colors: {
        green: {
          10: '#A8B693',
        },
        gray: {
          10: '#A89E94',
        },
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
